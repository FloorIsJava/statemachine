//! Provides utilities for working with statemachines.

#[doc(inline)]
pub use statemachine_macro::statemachine;