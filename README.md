# statemachine

A Rust crate providing utilities for working with state machines. Documentation is available on [docs.rs](https://docs.rs/statemachine).

**NOTE:** In the future, this module will contain more machines and automata. Currently it only adds a way to define statemachines.

## Usage

Add this to your `Cargo.toml`:
```toml
[dependencies]
statemachine = "0.1"
```

## Getting Started

```rs
use statemachine::statemachine;

statemachine! {
    #[derive(Default)]
    struct Foo {}

    enum FooState consumes [char] from Start accepts [NonEmpty];

    Start => {
        char match _ => NonEmpty
    },

    NonEmpty => {
        _ => NonEmpty
    }
}

fn main() {
    let mut foo: Foo = Default::default();
    assert!(!foo.is_accepting());
    foo.consume('a');
    assert!(foo.is_accepting());
    foo.consume('b');
    assert!(foo.is_accepting());
    foo.consume('c');
    assert!(foo.is_accepting());
}
```

## License

This crate is published under the terms of the MIT license. See the LICENSE file for details.